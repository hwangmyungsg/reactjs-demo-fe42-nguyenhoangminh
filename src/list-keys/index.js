import React, { Component } from 'react'

export default class ListKeys extends Component {
    constructor(props) {
        super(props);

        this.state = {
            listProduct: [
                {id: 1, name: 'iPhone X', price: 20000, imgUrl: 'https://bachlongmobile.com/media/catalog/product/cache/2/image/040ec09b1e35df139433887a97daa66f/6/3/636483223586180190_3_13_1.jpg'},
                {id: 2, name: 'iPhone X Pro', price: 25000, imgUrl: 'https://www.gizmochina.com/wp-content/uploads/2019/09/Apple-iPhone-11-Pro-Max-1-500x500.jpg'},
                {id: 3, name: 'iPhone 6', price: 14000, imgUrl: 'https://cdn.nguyenkimmall.com/images/thumbnails/696/522/detailed/61/IPHONE-6-GRAY_knod-gq.jpg'},
                {id: 4, name: 'iPhone 6s', price: 15000, imgUrl: 'https://cdn.nguyenkimmall.com/images/thumbnails/696/522/detailed/61/IPHONE-6-GRAY_knod-gq.jpg'},
                {id: 5, name: 'iPhone 7 Plus', price: 16000, imgUrl: 'https://hoanghamobile.com/Uploads/Originals/2016/09/08/201609081107459092_black.jpg;width=820;height=550;watermark=logo;crop=auto;format=jpg'},
            ],
        }
    }

    renderTable = () => {
        let { listProduct } = this.state;
        // return this.state.listProduct.map((product) => {
        return listProduct.map((product) => {
            return (
                <tr key={product.id}>
                    <td>{product.id}</td>
                    <td><img src={product.imgUrl} style={{width: '100px'}} alt='img' /></td>
                    <td>{product.name}</td>
                    <td>{product.price}</td>
                </tr>
            )
        })
    }

    render() {
        return (
            <div>
                <h2 className='title'>* ListKeys</h2>
                <table className='table'>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Img</th>
                            <th>Product</th>
                            <th>Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.renderTable()}
                    </tbody>
                </table>
            </div>
        )
    }
}
