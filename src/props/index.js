import React, { Component } from 'react'
import ClassProps from './classProps'
import FunctionProps from './functionProps'

export default class Props extends Component {
    constructor(props) {
        super(props);

        this.state = {
            username:   'minhnh',
            codeClass:  'FE42'
        };
    }


    render() {
        const { username, codeClass } = this.state;

        return (
            <div>
                <ClassProps name={username} codeClass={codeClass} />
                <ClassProps name='Hwang Myung' codeClass='FE40' />

                {/* <FunctionProps name={username} /> */}

                <FunctionProps>
                    <div>
                        <h3>Demo FunctionProps</h3>
                        <p>loremmmmmmmmmmmmmm</p>
                    </div>
                </FunctionProps>
            </div>
        )
    }
}
