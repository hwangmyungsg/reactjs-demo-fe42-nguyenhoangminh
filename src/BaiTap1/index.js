import React, {Component} from 'react';
import Header from './header';
import Content from './content';
import Sidebar from './sidebar';
import Footer from './footer';
import './baitap1.css';

export default class BaiTap1 extends Component {
    render() {
        return (
            <div>
                {/* <div>Bai tap 1</div> */}
                {/* <h1>Testing</h1> */}
                <Header/>
                <div className='contents'>
                    <Content/>
                    <Sidebar/>
                </div>
                <Footer/>
            </div>
        );
    }
}

// export default BaiTap1;