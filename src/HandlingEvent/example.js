import React, { Component } from 'react'

export default class ExampleHandlingEvent extends Component {
    username    = 'Hwang Myung';
    isLogin     = false;

    renderHTML = () => {
        // if (this.isLogin) {
        //     return <p>Hello {this.username}</p>;
        // } else {
        //     return <button className='btn btn-info' onClick={this.onclickLogin}>Login</button>;
        // }

        return (this.isLogin) ? (
            <p>Hello {this.username}</p>
        ) : (
            <button className='btn btn-info' onClick={this.onclickLogin}>Login</button>
        );
    }

    onclickLogin = () => {
        this.Login = !this.Login;
        // console.log(this.Login);
        this.render();
    }



    render() {
        // console.log('render');
        
        return (
            <div>
                <h2 className='title'>* ExampleHandlingEvent</h2>
                {this.renderHTML()}
            </div>
        );
    }
}
