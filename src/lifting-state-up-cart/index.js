import React, { Component } from "react";
import DanhSachSanPham from "./danh-sach-san-pham";
import Modal from "./modal";
import data from './data.json';

export default class LiftingStateUpCart extends Component {
	constructor(props) {
		super(props);

		this.state = {
			listProduct: 		data,
			detailProduct:		data[0],
			selectedProduct: 	[],
		};
	}

	handleDetailProduct = (product) => {
		// console.log('index.js: ', product);
		this.setState({
			detailProduct: product,
		})
	}

	count

	findIndexProductInCart = (id) => {
		return this.state.selectedProduct.findIndex(product => {
			return product.maSP === id;
		});
	}

	handleAddCart = (product) => {
		let currentListProduct = this.state.selectedProduct;

		// Method 1
		// let isExistProduct = false;
		// currentListProduct.forEach(item => {
		// 	// TH: Đã có Product trong Cart
		// 	if (item.maSP === product.maSP) {
		// 		item.soLuong 	+= 1;
		// 		isExistProduct 	= true;
		// 	}
		// });
		// let selectedProductTemp = [	...currentListProduct];
		// if (isExistProduct === false) {
		// 	product.soLuong = 1;				// Add product mới; SET số lượng = 1
		// 	selectedProductTemp.push(product);
		// }

		// Method 2: findIndex --- gọn hơn
		let selectedProductTemp 	= [	...currentListProduct];
		let positionProductInCart 	= this.findIndexProductInCart(product.maSP);
		if (positionProductInCart !== -1) {
			// Đã tồn tại product trong Cart => +1 soLuong
			selectedProductTemp[positionProductInCart].soLuong += 1;
		} else {
			product.soLuong 		= 1;		// Add product mới; SET số lượng = 1
			// selectedProductTemp 	= [	...currentListProduct, product];
			selectedProductTemp.push(product);
		}



		// Update cart
		this.setState({
			selectedProduct: selectedProductTemp,
		}, 
			() => {
				console.log('Add product to Cart successfully');
			}
		);
	}

	// handleDecreaseProductCart = (product) => {
	// 	let currentListProduct = this.state.selectedProduct;
	// 	currentListProduct.forEach(item => {
	// 		if (item.maSP === product.maSP) {
	// 			item.soLuong 	-= 1;
	// 		}
	// 	});

	// 	// Update cart
	// 	this.setState({
	// 		selectedProduct: currentListProduct,
	// 	}, 
	// 		() => {
	// 			console.log('Decrease product from Cart successfully');
	// 		}
	// 	);
	// }

	handleRemoveProductFromCart = (product) => {
		// Method 1
		// let currentListProduct = this.state.selectedProduct;
		// currentListProduct.some((item, index) => {
		// 	// Remove product in cart
		// 	if (item.maSP === product.maSP) {
		// 		currentListProduct.splice(index, 1);
		// 		return true;
		// 	}
		// 	return false;
		// });
		// this.setState({
		// 	selectedProduct: currentListProduct,
		// }, 
		// 	() => {
		// 		console.log('Remove product from Cart successfully');
		// 	}
		// );

		// Method 2
		const indexProductInCart = this.findIndexProductInCart(product.maSP);
		if (indexProductInCart !== -1) {
			let currentListProduct = this.state.selectedProduct;
			// Cach 1
			// currentListProduct.splice(indexProductInCart, 1);

			// Cach 2
			currentListProduct = currentListProduct.filter(item => {
				return item.maSP !== product.maSP;
			});

			// Update cart
			this.setState({
				selectedProduct: currentListProduct,
			}, 
				() => {
					console.log('Remove product from Cart successfully');
				}
			);
		}
	}

	handleUpdateQuantityProduct = (flag, product) => {
		const indexProductInCart 	= this.findIndexProductInCart(product.maSP);
		let selectedProductTemp 	= [...this.state.selectedProduct];
		let message;
		if (flag) {
			// Increase quantity product
			selectedProductTemp[indexProductInCart].soLuong += 1;
			message = 'increase quantity product successfullly';
		} else {
			// Decrease quantity product
			if (selectedProductTemp[indexProductInCart].soLuong > 1) {
				selectedProductTemp[indexProductInCart].soLuong -= 1;
				message = 'decrease quantity product successfullly';
			}
		}

		// update Cart
		this.setState({
			selectedProduct: selectedProductTemp,
		}, () => {
			console.log(message);
		});
	}

	countQuantityItemCard = () => {
		return this.state.selectedProduct.reduce((tong, product) => {
			return tong += product.soLuong;
		}, 0);
	}

	render() {
		const { listProduct, detailProduct, selectedProduct } = this.state;

		return (
			<div>
				<div className="container my-3">
					<h3 className="title" style={{display: 'inline-block'}}>Bài tập giỏ hàng</h3>
					<button
						className="btn btn-danger"
						data-toggle="modal"
						data-target="#modelId"
						style={{float: 'right'}}
					>
						<i className="fa fa-shopping-cart mr-1"></i>
						{/* Giỏ hàng ({this.state.selectedProduct.length}) */}
						Giỏ hàng ({this.countQuantityItemCard()})
					</button>
				</div>

				<DanhSachSanPham 
					listProduct={listProduct}
					detailProduct={this.handleDetailProduct}
					handleAddCart={this.handleAddCart}
				/>

				<div className='container mt-5'>
					<div className="row">
						<div className="col-sm-5">
							<img className="img-fluid" src={detailProduct.hinhAnh} alt={detailProduct.tenSP} style={{height: '368px'}}/>
						</div>
						
						<div className="col-sm-7">
							<h3>Thông số kỹ thuật</h3>
							<table className="table">
								<tbody>
									<tr>
										<td>Màn hình</td>
										<td>{detailProduct.manHinh}</td>
									</tr>
									<tr>
										<td>Hệ điều hành</td>
										<td>{detailProduct.heDieuHanh}</td>
									</tr>
									<tr>
										<td>Camera trước</td>
										<td>{detailProduct.cameraTruoc}</td>
									</tr>
									<tr>
										<td>Camera sau</td>
										<td>{detailProduct.cameraSau}</td>
									</tr>
									<tr>
										<td>RAM</td>
										<td>{detailProduct.ram}</td>
									</tr>
									<tr>
										<td>ROM</td>
										<td>{detailProduct.rom}</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>

				<Modal 
					listProduct					={selectedProduct}
					handleRemoveProductFromCart	={this.handleRemoveProductFromCart}
					// handleAddCart				={this.handleAddCart}
					// handleDecreaseProductCart	={this.handleDecreaseProductCart}
					handleUpdateQuantity		={this.handleUpdateQuantityProduct}
				/>
			</div>
		);
	}
}
