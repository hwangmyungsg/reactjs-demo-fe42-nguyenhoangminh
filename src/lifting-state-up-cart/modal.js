import React, { Component } from "react";
import * as Helpers from './../helpers';

export default class Modal extends Component {
	// increaseQuantityProduct = (product) => {
	// 	this.props.handleAddCart(product);
	// }

	// decreaseQuantityProduct = (product) => {
	// 	this.props.handleDecreaseProductCart(product);
	// }

	// removeProductFromCart = (product) => {
	// 	this.props.handleRemoveProductFromCart(product);
	// }

	renderListProduct = () => {
		// const { listProduct } = this.state;
		const { listProduct } = this.props;
		if (listProduct.length > 0) {
			return listProduct.map(item => {
				return (
					<tr key={item.maSP}>
						<td className='text-center align-middle'>{item.maSP}</td>
						<td className='align-middle'>{item.tenSP}</td>
						<td className='text-center align-middle'>
							<img src={item.hinhAnh} alt={item.tenSP} height={50} />
						</td>
						<td className='text-center align-middle'>
							{/* <button className='btn btn-sm btn-outline-danger' onClick={() => {this.decreaseQuantityProduct(item)}} disabled={item.soLuong === 1}>-</button>
							<span className='mx-2'>{item.soLuong}</span>
							<button className='btn btn-sm btn-outline-success' onClick={() => {this.increaseQuantityProduct(item)}}>+</button> */}
							<button className='btn btn-sm btn-outline-danger' onClick={() => {this.props.handleUpdateQuantity(false, item)}} disabled={item.soLuong === 1}>-</button>
							<span className='mx-2'>{item.soLuong}</span>
							<button className='btn btn-sm btn-outline-success' onClick={() => {this.props.handleUpdateQuantity(true, item)}}>+</button>
						</td>
						<td className='text-right align-middle'>{Helpers.moneyFormat(item.giaBan)}</td>
						<td className='text-right align-middle'>{Helpers.moneyFormat(item.giaBan * item.soLuong)}</td>
						<td className='text-center align-middle'>
							{/* <button className="btn btn-danger" onClick={() => {this.removeProductFromCart(item)}}><i className="fa fa-trash"></i></button> */}
							<button className="btn btn-danger" onClick={() => {this.props.handleRemoveProductFromCart(item)}}><i className="fa fa-trash"></i></button>
						</td>
					</tr>
				);
			});
		} else {
			return(
				<tr>
					<td colSpan='7' className='font-weight-light'>Chưa có sản phẩm nào trong giỏ hàng</td>
				</tr>
			)
		}
		
	}

	render() {
		return (
			<div className="modal fade" id="modelId" tabIndex={-1} role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
				<div className="modal-dialog" style={{ maxWidth: "1000px" }} role="document">
					<div className="modal-content">
						<div className="modal-header">
							<h5 className="modal-title"><i className="fa fa-shopping-cart mr-1"></i> Giỏ hàng</h5>
							<button type="button" className="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">×</span>
							</button>
						</div>
						<div className="modal-body">
							<table className="table mb-0">
								<thead>
									<tr>
										<th style={{width: '126px'}}>Mã sản phẩm</th>
										<th>Tên sản phẩm</th>
										<th className='text-center' style={{width: '93px'}}>Hình ảnh</th>
										<th className='text-center' style={{width: '111px'}}>Số lượng</th>
										<th className='text-right' style={{width: '139px'}}>Đơn giá</th>
										<th className='text-right' style={{width: '139px'}}>Thành tiền</th>
										<th style={{width: '96px'}} className='text-center'><i className='fa fa-cogs'></i></th>
									</tr>
								</thead>
								<tbody>
									{this.renderListProduct()}
								</tbody>
							</table>
						</div>
						<div className="modal-footer">
							<button type="button" className="btn btn-secondary" data-dismiss="modal">Đóng</button>
							<button type="button" className="btn btn-primary" disabled={this.props.listProduct.length === 0}>Mua hàng</button>
						</div>
					</div>
				</div>
			</div>
		);
	}
}