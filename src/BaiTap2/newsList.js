import React from 'react';
import NewsItem from './newsItem';

export default function NewsList() {
    return (
        <div className='newsList row'>
            <NewsItem/>
            <NewsItem/>
            <NewsItem/>
        </div>
    );
}