import React, { Component } from "react";
import SanPham from './san-pham';
// import data from './data.json';

export default class DanhSachSanPham extends Component {
	// constructor(props) {
	// 	super(props);

	// 	// this.state = {
	// 	// 	listProduct: 	this.props.listProduct,
	// 	// 	// detailProduct: 	this.props.listProduct[0],		// Default detail product: first product
	// 	// };
	// }

	renderProductList = () => {
		// const { listProduct } = this.state;
		const { listProduct } = this.props;

		return listProduct.map((product, index) => {
			return (
				<SanPham 
					key={index} 
					product={product} 
					detailProduct={this.detailProduct}
					// handleDetailProduct={this.props.handleDetailProduct}
					handleAddCart={this.handleAddCart}
				/>
			);
		})
	}

	detailProduct = (product) => {
		// console.log('component cha - danh sach san pham: ', product);
		// this.setState({
		// 	detailProduct: product,
		// })
		this.props.detailProduct(product);
	}

	handleAddCart = (product) => {
		// console.log('handleAddCart - danh sach san pham: ', product);
		this.props.handleAddCart(product);
	}

	render() {
		// const { detailProduct } = this.state;

		return (
			<div className="container mb-3">
				<div className="row">
					{this.renderProductList()}
				</div>
			</div>
		);
	}
}
