import React, { Component } from 'react'

export default class HandlingEvent extends Component {
    handleOnClick = () => {
        console.log('handleOnClick');
        
    }

    handleOnClickWithParams = (ok) => {
        console.log(ok);
    }



    render() {
        return (
            <div>
                <h3 className='title'>* Handling event</h3>
                <button onClick={this.handleOnClick} className='btn btn-primary'>Click here</button>
                <button onClick={() => {this.handleOnClickWithParams('ok2')}} className='btn btn-primary ml-5'>Handling event with params</button>

                <br/>
                <br/>
                <h2>It is {new Date().toLocaleTimeString()}.</h2>
            </div>
        )
    }
}
