import React from 'react';

export default function FunctionProps(props) {
    return (
        <div>
            <h3>FunctionProps</h3>
            <div>
                Hello {props.username}
            </div>

            {props.children}
        </div>
    )
}
