import React, { Component } from 'react'

export default class RederingElements extends Component {
    username    = 'Hwang Myung';
    class       = 'FE42';

    renderInfo = () => {
        return (<p>Username: {this.username} - Class: {this.class}</p>);
    }

    render() {
        return (
            <div>
                <h3 className='title'>RenderingElements</h3>
                {/* <p>Username: {this.username}</p>
                <p>Class: {this.class}</p> */}
                {this.renderInfo()}
            </div>
        )
    }
}