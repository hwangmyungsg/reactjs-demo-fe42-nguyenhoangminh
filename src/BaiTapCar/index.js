import React, { Component } from 'react'

export default class BaiTapCar extends Component {
    constructor(props) {
        super(props);

        this.state = {
            // currentCar: './img/imgRedCar.jpg',
            currentImgUrl: './img/imgRedCar.jpg',
            currentColorCar: 'Red car',
        }
    }

    // setCurrentCar = (imgUrl, color) => {
    //     this.setState({
    //         currentImgUrl: imgUrl,
    //         currentColorCar: color,
    //     })
    // }
    setCurrentCar = (color) => {
        const baseImgUrl = './img';
        let imgUrl, colorCar;
        // debugger;
        if (color === 'red') {
            imgUrl = baseImgUrl+'/imgRedCar.jpg';
            colorCar = 'Red car';
        } else if(color === 'silver') {
            imgUrl = baseImgUrl+'/imgSilverCar.jpg';
            colorCar = 'Silver car';
        } else if(color === 'black') {
            imgUrl = baseImgUrl+'/imgBlackCar.jpg';
            colorCar = 'Black car';
        }

        this.setState({
            currentImgUrl: imgUrl,
            currentColorCar: colorCar,
        })
    }

    render() {
        return (
            <div className='container'>
                <h2 className='title'>* BaiTapCar</h2>

                <div className='row'>
                    <div className='col-sm-6'>
                        <img className='img-fluid' alt={this.state.currentColorCar} src={this.state.currentImgUrl}/>
                    </div>

                    <div className='col-sm-6'>
                        {/* <button className='btn btn-danger mr-2' onClick={() => {this.setCurrentCar('./img/imgRedCar.jpg', 'Red car')}}>Red</button>
                        <button className='btn btn-light color-silver mr-2' onClick={() => {this.setCurrentCar('./img/imgSilverCar.jpg', 'Silver car')}}>Silver</button>
                        <button className='btn btn-dark mr-2' onClick={() => {this.setCurrentCar('./img/imgBlackCar.jpg', 'Black car')}}>Black</button> */}
                        <button className='btn btn-danger mr-2' onClick={() => {this.setCurrentCar('red')}}>Red</button>
                        <button className='btn btn-light color-silver mr-2' onClick={() => {this.setCurrentCar('silver')}}>Silver</button>
                        <button className='btn btn-dark mr-2' onClick={() => {this.setCurrentCar('black')}}>Black</button>
                    </div>
                </div>
            </div>
        )
    }
}
