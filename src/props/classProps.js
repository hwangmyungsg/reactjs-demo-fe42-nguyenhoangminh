import React, { Component } from 'react'

export default class ClassProps extends Component {
    render() {
        return (
            <div>
                <h3>ClassProps</h3>
                <div>Hello <span style={{'fontWeight': 'bold'}}>{this.props.name}</span> - class: {this.props.codeClass}</div>
            </div>
        )
    }
}
