import React, { Component } from "react";

export default class SanPham extends Component {
	handleDetail = () => {
		const currentProduct = this.props.product;
		// funtion ở component cha
		this.props.detailProduct(currentProduct);
	}

	handleAddCart = () => {
		const currentProduct = this.props.product;
		this.props.handleAddCart(currentProduct);
		// console.log(currentProduct);
	}

	render() {
		const { product } = this.props;

		return (
			<div className="col-sm-4">
				<div className="card">
				<img className="card-img-top" src={product.hinhAnh} alt={product.tenSP} style={{height: '384px'}}/>
				<div className="card-body">
					<h4 className="card-title">{product.tenSP}</h4>
					{/* <button className="btn btn-success" onClick={() => {this.handleDetail()}}>Chi tiết</button> */}
					<button className="btn btn-success" onClick={this.handleDetail}>Chi tiết</button>
					<button className="btn btn-danger ml-1" onClick={this.handleAddCart}>Thêm giỏ hàng</button>
				</div>
				</div>
			</div>
		);
	}
}
