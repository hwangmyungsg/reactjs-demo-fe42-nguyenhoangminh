import React from 'react';
// import logo from './logo.svg';
import './App.css';
// import BaiTap1 from './BaiTap1';
// import BaiTap2 from './BaiTap2';
// import RederingElements from './RenderingElements';
// import HandlingEvent from './HandlingEvent';
// import ExampleHandlingEvent from './HandlingEvent/example';
// import State from './state';
// import ListKeys from './list-keys';
// import BaiTapCar from './BaiTapCar';
// import Props from './props';
// import LiftingStateUp from './lifting-state-up';
import LiftingStateUpCart from './lifting-state-up-cart';

function App() {
  return (
    <div>
      {/* <BaiTap1 /> */}
      {/* <BaiTap2 /> */}
      {/* <RederingElements/>
      <hr/>
      <HandlingEvent/>
      <hr/>
      <ExampleHandlingEvent/>
      <hr/>
      <State/>
      <hr/>
      <ListKeys/>
      <br/>
      <BaiTapCar/>
      <br/>
      <Props/>
      <br/> */}
      {/* <LiftingStateUp/> */}
      {/* <br/> */}
      <LiftingStateUpCart/>
    </div>

  );
}

export default App;
