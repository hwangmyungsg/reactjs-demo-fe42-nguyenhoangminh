import React, { Component } from 'react'

export default class State extends Component {
    constructor(props) {
        super(props);

        this.state = {
            username:   'Hwang Myung',
            isLogin:    false,
        };
    }

    renderHTML = () => {
        // if (this.isLogin) {
        //     return <p>Hello {this.username}</p>;
        // } else {
        //     return <button className='btn btn-info' onClick={this.onclickLogin}>Login</button>;
        // }

        return (this.state.isLogin) ? (
            <p>Hello {this.state.username}, <button onClick={this.onclickLogin} className='btn btn-warning'>Logout</button></p>
        ) : (
            <button className='btn btn-info' onClick={this.onclickLogin}>Login</button>
        );
    }

    onclickLogin = () => {
        // this.state.Login = !this.state.isLogin;
        this.setState({
            isLogin: !this.state.isLogin,
        })
    }



    render() {
        // console.log('render');

        return (
            <div>
                <h2 className='title'>* State</h2>
                {this.renderHTML()}
            </div>
        )
    }
}
