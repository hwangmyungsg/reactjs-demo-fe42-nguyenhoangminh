function moneyFormat(input) {
    if (input == null) {
        return 0;
    }
    return String(input).replace(/(.)(?=(\d{3})+$)/g,'$1.')+ ' đ';
}

export {
    moneyFormat,
};