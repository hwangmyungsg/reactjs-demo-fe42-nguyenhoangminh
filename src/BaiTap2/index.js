// import React, {Component} from 'react';
import React from 'react';
// import './baiTap2.css';
import Header from './header';
import Carousel from './carousel';
import WhatWeDo from './whatWeDo';
import Contact from './contact';
import NewsList from './newsList';
import Footer from './footer';

export default function BaiTap2() {
    /**
     * LAYOUT:
        Header
        Carousel
        ---.contents
        WhatWeDo
        Contact
        ---
        ListNews
            + ItemNews
        Footer
     */
    return (
        <div>
            <Header/>
            <Carousel/>
            <div className='container'>
                <div className='row'>
                    <WhatWeDo/>
                    <Contact/>
                </div>

                <NewsList/>
            </div>
            <Footer/>
        </div>
    );
}